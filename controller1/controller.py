from controller_interface import ControllerInterface
from math import pow, sqrt
import numpy

class Controller(ControllerInterface):
    
    first_time = 1
    last_distance_enemy_mothership = 0
    number_of_actions = 5
        
    def take_action(self, weights: tuple) -> int:
        
        """
            :return: An integer corresponding to an action:
            1 - Right
            2 - Left
            3 - Accelerate forward
            4 - Discharge
            5 - Nothing
            """
        
        features = self.compute_features(dict)
        actions_q = [0] * self.number_of_actions
        weight_index = 0
        
        for i in range(0, len(actions_q)):
            
            action = weights[weight_index]
            weight_index += 1
            
            for feature in features:
                action += weights[weight_index] * feature
                weight_index += 1
            
            actions_q[i] = action
        
        return actions_q.index(max(actions_q)) + 1


    def compute_features(self, sensors: dict) -> tuple:
        """
        This function should take the raw sensor information of the ship (see below) and compute useful features
        for selecting an action.
        The ship has the following sensors:

        :param sensors: contains:
             'asteroid_position': (x, y)
             'asteroid_velocity': (n, m)
             'asteroid_resources': 0 - ???
             'align_asteroid': 0 or 1
             'align_mothership': 0 or 1
             'drill_angle': angle in rad 
             'drill_position': (x, y) 
             'drill_velocity': (n, m)
             'drill_mothership_position': (x, y)
             'drill_resources': 0 - ???
             'drill_touching_asteroid': 0 or 1
             'drill_touching_mothership': 0 or 1
             'drill_discharge_cooldown': 0 - COOLDOWN (defined in config.py)
             'drill_edge_position': (x, y)
             'drill_gas': 0 - MAX_GAS (defined in config.py)
             'enemy_1_drill_angle': angle in rad
             'enemy_1_drill_position': (x, y)
             'enemy_1_drill_velocity': (n, m)
             'enemy_1_drill_mothership_position': (x, y)
             'enemy_1_drill_resources': 0 - 0  
             'enemy_1_drill_touching_asteroid': 0 or 1 
             'enemy_1_drill_touching_mothership': 0 or 1 
             'enemy_1_drill_discharge_cooldown': 0 - COOLDOWN  (defined in config.py)
             'enemy_1_drill_edge_position': (x, y)
             'enemy_1_drill_gas': 0 - MAX_GAS (defined in config.py)
        :return: A Tuple containing the features you defined
        """
        distance_enemy_mothership = self.diff_distance_enemy_mothership()
        algin_enemy_mother = self.aligned_distance_enemy_mothership()
        
        first_time = 0
        
        return distance_enemy_mothership, algin_enemy_mother

    
    def diff_distance_enemy_mothership(self):
        
        mx, my = self.sensors['enemy_1_drill_mothership_position']
        dx, dy = self.sensors['drill_position']
        
        distance = sqrt(pow(mx-dx, 2) + pow(my-dy, 2))
        
        if self.first_time:
            diff = distance
            self.last_distance_enemy_mothership = distance
            return diff
        else:
            diff = distance - last_distance_enemy_mothership
            self.last_distance_enemy_mothership = distance
            return diff

    def aligned_distance_enemy_mothership(self):
        
        align = self.sensors['align_enemy_mothership']
        return align


    def learn(self, weights: tuple):
        """
        IMPLEMENT YOUR LEARNING METHOD (i.e. YOUR LOCAL SEARCH ALGORITHM) HERE

        HINTS: You can call self.run_episode (see controller_interface.py) to evaluate a given set of weights.
               The variable self.episode shows the number of times run_episode method has been called 

        :param weights: initial weights of the controller (either loaded from a file or generated randomly)
        :return: the best weights found by your learning algorithm, after the learning process is over
        """
        
        print("initial weights", weights)
        
        previous_score = self.run_episode(weights)
        print("initial value", previous_score)
        print()
        print()
        neighbor_list = self.generate_neighbors(weights)
        print("list of neighbors: ", neighbor_list)
        print()
        print()

        high_score = previous_score
        
        for neighbor in neighbor_list:
            print("neighbor: ", neighbor)
            
            
            neighbor_score = self.run_episode(neighbor)
            print("neigh score", neighbor_score)
            if neighbor_score > high_score:
                weights = neighbor
                high_score = neighbor_score
                print("New high score!!! ", high_score)
                print()
            print()
            print()
           
        
        print("initial value", previous_score)
        print("current high score", high_score)
        
        print()
        print()
        
        while previous_score < high_score:
        
            previous_score = high_score
        
            neighbor_list = self.generate_neighbors(weights)
            
            for neighbor in neighbor_list:
                neighbor_value = self.run_episode(neighbor)
                print("neigh score aa", neighbor_value)
                if neighbor_value > high_score:
                    weights = neighbor
                    high_score = neighbor_value
                    print("New high score!!! ", high_score)
                    print()

            print("value", previous_score)
            print("value neigh", high_score)

            
        print("final weights", weights)

    def generate_neighbors(self, weights: tuple) -> tuple:
        
        neighbohr_number = 50
        
        neighbor_list = []
        for x in range(0, neighbohr_number):
            random_list = numpy.random.normal(0, 0.5, 15)
            neighbor = random_list + list(weights)
            neighbor_list.append(neighbor)
        
        return neighbor_list
