import pymunk
from body import *
from pygame.color import THECOLORS
from random import randint, sample
from controller_interface import ControllerInterface
import config


class Player:
    def __init__(self, drill, mothership):
        self.drill = drill
        self.mothership = mothership


class Simulation(pymunk.Space):

    WIDTH = config.WIDTH
    HEIGHT = config.HEIGHT
    OFFSET = 100

    def __init__(self, *controllers: ControllerInterface):
        super().__init__()

        # Controller_list
        self.controllers = controllers

        self.player_list = []

        # init objects
        self.drill_1 = SpaceDrill(config.SHIP_1_STARTING_POS[0], config.SHIP_1_STARTING_POS[1], self, THECOLORS['red'],
                                  config.SHIP_1_STARTING_ANGLE)
        self.drill_2 = SpaceDrill(config.SHIP_2_STARTING_POS[0], config.SHIP_2_STARTING_POS[1], self, THECOLORS['blue'],
                                  config.SHIP_2_STARTING_ANGLE)
        self.mother_ship_1 = MotherShip(self, THECOLORS['red'], self.drill_1,
                                        config.MOTHERSHIP_1_STARTING_POS, config.MOTHERSHIP_1_STARTING_ANGLE)
        self.mother_ship_2 = MotherShip(self, THECOLORS['blue'], self.drill_2,
                                        config.MOTHERSHIP_2_STARTING_POS, config.MOTHERSHIP_2_STARTING_ANGLE)

        self.asteroid = self.spawn_asteroid()

        # fill player list
        self.player_list.append(Player(self.drill_1, self.mother_ship_1))
        self.player_list.append(Player(self.drill_2, self.mother_ship_2))

        # init collision calls
        drill_asteroid_collision = self.add_collision_handler(DRILL_COLLISION_TYPE, ASTEROID_COLLISION_TYPE)
        drill_asteroid_collision.begin = asteroid_set_joint
        drill_asteroid_collision.separate = asteroid_drill_remove_joint
        drill_asteroid_collision.pre_solve = asteroid_drill_point_update

        drill_mothership_collision = self.add_collision_handler(DRILL_COLLISION_TYPE, MOTHER_SHIP_COLLISION_TYPE)
        drill_mothership_collision.begin = drill_mother_ship_set_joint
        drill_mothership_collision.separate = drill_mother_ship_remove_joint
        drill_mothership_collision.pre_solve = drill_mother_ship_point_update

        mother_ship_asteroid_collision = self.add_collision_handler(MOTHER_SHIP_COLLISION_TYPE, ASTEROID_COLLISION_TYPE)
        mother_ship_asteroid_collision.begin = asteroid_mother_ship_collision

        pass

    def frame_step(self, *actions):

        # Useful for when multiple controllers are made
        for i in range(0, len(actions)):
            self.player_list[i].drill.frame_step(actions[i])

        # Decreases drills velocity over time
        for player in self.player_list:
            player.drill.velocity *= 0.99

        # Check if asteroid is set for destruction
        if self.asteroid.set_for_destruction:
            self.destroy_asteroid()
            self.asteroid = self.spawn_asteroid()

        # Updates asteroid resources score
        for player in self.player_list:
            if player.drill.touching_asteroid and player.drill.joint_blocked_time == 0:
                player.drill.resources += 2
                self.asteroid.resources -= 2

        if self.asteroid.resources <= 0:
            self.asteroid.set_for_destruction = True

        # Updates mothership resource score
        for player in self.player_list:
            if player.drill.touching_mothership:
                if player.drill.jointed_mothership_body.owner is player.drill:
                    player.drill.gasoline = player.drill.MAX_GASOLINE
                if player.drill.jointed_mothership_body.owner.resources > 0 and player.drill.joint_blocked_time == 0:
                    player.drill.resources += 1
                    player.drill.jointed_mothership_body.owner.resources -= 1

        # Ship Velocity bug prevention
        for player in self.player_list:
            if player.drill.jointed_with_mothership:
                player.drill.velocity = 0, 0

        # Check if asteroid is out of bounds:
        if self.asteroid.position[0] < -self.OFFSET or self.asteroid.position[0] > self.WIDTH+self.OFFSET:
            self.asteroid.set_for_destruction = True
        if self.asteroid.position[1] < -self.OFFSET or self.asteroid.position[1] > self.HEIGHT+self.OFFSET:
            self.asteroid.set_for_destruction = True

        self.step(1.0 / 10.0)

        return self.drill_1.sensors

    def spawn_asteroid(self):
        asteroid_x = sample([0-50, randint(0, self.WIDTH)+50], 1)[0]
        asteroid_y = sample([0-50, self.HEIGHT+50], 1)[0]
        asteroid_rad_x = randint(30, 50)
        asteroid_rad_y = randint(30, 50)

        asteroid = Asteroid(asteroid_x, asteroid_y, asteroid_rad_x, asteroid_rad_y, self)

        asteroid.apply_impulse_at_local_point(
            asteroid.world_to_local((self.WIDTH/2, self.HEIGHT/2)).normalized()*20*asteroid.ASTEROID_MASS, (0, 0))

        return asteroid

    def destroy_asteroid(self):

        if self.drill_1.jointed_with_asteroid:
            self.drill_1.velocity = self.asteroid.velocity
        if self.drill_2.jointed_with_asteroid:
            self.drill_2.velocity = self.asteroid.velocity

        try:
            self.remove(self.drill_1.asteroid_joint)
        except (TypeError, KeyError):
            pass
        try:
            self.remove(self.drill_2.asteroid_joint)
        except (TypeError, KeyError):
            pass
        try:
            self.remove(self.asteroid.asteroid_shape)
        except (TypeError, KeyError):
            pass
        try:
            self.remove(self.asteroid.asteroid_shape)
        except (TypeError, KeyError):
            pass

        self.drill_1.touching_asteroid = False
        self.drill_1.jointed_with_asteroid = False
        self.drill_1.asteroid_joint = None
        self.drill_2.touching_asteroid = False
        self.drill_2.jointed_with_asteroid = False
        self.drill_2.asteroid_joint = None

    def reset(self):

        # reset objects
        self.drill_1.position = config.SHIP_1_STARTING_POS
        self.drill_2.position = config.SHIP_2_STARTING_POS

        self.drill_1.velocity = 0, 0
        self.drill_2.velocity = 0, 0

        self.drill_1.angle = config.SHIP_1_STARTING_ANGLE
        self.drill_2.angle = config.SHIP_2_STARTING_ANGLE

        self.drill_1.resources = 0
        self.drill_2.resources = 0

        self.drill_1.gasoline = self.drill_1.MAX_GASOLINE
        self.drill_2.gasoline = self.drill_2.MAX_GASOLINE

        self.drill_1.discharge_cooldown = 0
        self.drill_2.discharge_cooldown = 0

        self.drill_1.joint_blocked_time = 0
        self.drill_2.joint_blocked_time = 0

        self.asteroid.set_for_destruction = True

        pass
